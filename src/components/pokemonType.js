import React from 'react';
import './styles/pokemonType.css';

export const PokemonType = ({ types }) => (
  <div data-testid="pokemon-type" className="pokemon-type-container">
    <div className="pokemon-type">
      {types && processTypes(types)}
    </div>
  </div>
);

const processTypes = (arg) => arg.map((item) => (
  <div key={item.type.name} id="type" className={item.type.name}>
    <div>{item.type.name}</div>
  </div>
));
