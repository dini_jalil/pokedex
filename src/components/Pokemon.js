import React, { useEffect, useState } from 'react';
import { getNewPokemon, updatePokeDex } from '../utils/pokemonClient';
import { PokemonPanel } from './pokemonPanel';
import { PokeDex } from './pokeDex';
import './styles/pokemon.css';

export const Pokemon = () => {
  const [pokedex, setPokedex] = useState([]);
  const [wildPokemon, setWildPokemon] = useState({});

  const generateNewPokemon = async () => {
    const newPokemon = await getNewPokemon();
    await setWildPokemon(newPokemon.data);
  };

  const catchPokemon = async (pokemon) => {
    setPokedex(updatePokeDex(pokedex, pokemon));
    await generateNewPokemon();
  };
  useEffect(async () => {
    await generateNewPokemon();
  }, []);

  return (
    <div className="panel-container">
      <div className="panel left ">
        <PokemonPanel
          wildPokemon={wildPokemon}
          catchPokemon={catchPokemon}
          newPokemon={generateNewPokemon}
        />
      </div>
      <div className="panel right">
        <PokeDex pokedex={pokedex} />
      </div>
    </div>
  );
};
