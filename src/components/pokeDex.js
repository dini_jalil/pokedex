import React from 'react';
import './styles/pokdex.css';
import { PokemonCard } from './pokemonCard';

export const PokeDex = ({ pokedex }) => (
  <div data-testid="pokedex" className="pokedex-container">
    {pokedex && pokedex.map((pokemon) => <PokemonCard wildPokemon={pokemon} />)}
  </div>
);
