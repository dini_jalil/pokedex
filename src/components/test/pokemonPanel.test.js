import 'jsdom-global/register';
import '@testing-library/jest-dom';
import React from 'react';
import {
  render, screen, fireEvent,
} from '@testing-library/react';
import { PokemonPanel } from '../pokemonPanel';

describe('pokemonPanel.js', () => {
  const mockPokemon = {
    name: 'bulbasaur',
    height: 7,
    weight: 69,
    sprites: {
      back_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png',
      back_female: null,
      back_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png',
      back_shiny_female: null,
      front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
      front_female: null,
      front_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png',
      front_shiny_female: null,
    },
    types: [
      {
        slot: 1,
        type: {
          name: 'grass',
          url: 'https://pokeapi.co/api/v2/type/12/',
        },
      },
      {
        slot: 2,
        type: {
          name: 'poison',
          url: 'https://pokeapi.co/api/v2/type/4/',
        },
      },
    ],
    stats: [
      {
        base_stat: 45,
        effort: 0,
        stat: {
          name: 'hp',
          url: 'https://pokeapi.co/api/v2/stat/1/',
        },
      },
      {
        base_stat: 49,
        effort: 0,
        stat: {
          name: 'attack',
          url: 'https://pokeapi.co/api/v2/stat/2/',
        },
      },
      {
        base_stat: 49,
        effort: 0,
        stat: {
          name: 'defense',
          url: 'https://pokeapi.co/api/v2/stat/3/',
        },
      },
      {
        base_stat: 65,
        effort: 1,
        stat: {
          name: 'special-attack',
          url: 'https://pokeapi.co/api/v2/stat/4/',
        },
      },
      {
        base_stat: 65,
        effort: 0,
        stat: {
          name: 'special-defense',
          url: 'https://pokeapi.co/api/v2/stat/5/',
        },
      },
      {
        base_stat: 45,
        effort: 0,
        stat: {
          name: 'speed',
          url: 'https://pokeapi.co/api/v2/stat/6/',
        },
      },
    ],
  };
  describe('GIVEN: valid wild pokemon', () => {
    it('THEN: renders pokemon card', () => {
      render(<PokemonPanel wildPokemon={mockPokemon} />);
      const pokemonCard = screen.getByTestId('pokemon-card');
      expect(pokemonCard).toBeInTheDocument();
    });
  });
  describe('GIVEN: new pokemon handler', () => {
    describe('WHEN: new pokemon button is clicked', () => {
      it('THEN: invokes new pokemon handler', () => {
        const mockHandler = jest.fn();
        render(<PokemonPanel
          wildPokemon={mockPokemon}
          newPokemon={mockHandler}
        />);
        const newPokemonButton = screen.getByText('Next Pokemon!');
        fireEvent.click(newPokemonButton);
        expect(mockHandler).toHaveBeenCalled();
      });
    });
  });
  describe('GIVEN: catch handler', () => {
    describe('WHEN: catch button is clicked', () => {
      it('THEN: invokes catch handler', () => {
        const mockCatchHandler = jest.fn();
        render(<PokemonPanel
          wildPokemon={mockPokemon}
          catchPokemon={mockCatchHandler}
        />);
        const newPokemonButton = screen.getByText('Catch');
        fireEvent.click(newPokemonButton);
        expect(mockCatchHandler).toHaveBeenCalled();
      });
    });
  });
});
