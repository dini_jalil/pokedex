import 'jsdom-global/register';
import React from 'react';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { PokemonCard } from '../pokemonCard';

const mockPokemon = {
  name: 'bulbasaur',
  height: 7,
  weight: 69,
  sprites: {
    back_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png',
    back_female: null,
    back_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png',
    back_shiny_female: null,
    front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
    front_female: null,
    front_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png',
    front_shiny_female: null,
  },
  types: [
    {
      slot: 1,
      type: {
        name: 'grass',
        url: 'https://pokeapi.co/api/v2/type/12/',
      },
    },
    {
      slot: 2,
      type: {
        name: 'poison',
        url: 'https://pokeapi.co/api/v2/type/4/',
      },
    },
  ],
  stats: [
    {
      base_stat: 45,
      effort: 0,
      stat: {
        name: 'hp',
        url: 'https://pokeapi.co/api/v2/stat/1/',
      },
    },
    {
      base_stat: 49,
      effort: 0,
      stat: {
        name: 'attack',
        url: 'https://pokeapi.co/api/v2/stat/2/',
      },
    },
    {
      base_stat: 49,
      effort: 0,
      stat: {
        name: 'defense',
        url: 'https://pokeapi.co/api/v2/stat/3/',
      },
    },
    {
      base_stat: 65,
      effort: 1,
      stat: {
        name: 'special-attack',
        url: 'https://pokeapi.co/api/v2/stat/4/',
      },
    },
    {
      base_stat: 65,
      effort: 0,
      stat: {
        name: 'special-defense',
        url: 'https://pokeapi.co/api/v2/stat/5/',
      },
    },
    {
      base_stat: 45,
      effort: 0,
      stat: {
        name: 'speed',
        url: 'https://pokeapi.co/api/v2/stat/6/',
      },
    },
  ],
};
describe('pokemonCard.jsx', () => {
  describe('PokemonCard', () => {
    describe('GIVEN: pokemon data', () => {
      describe('AND: pokemon data has been passed in', () => {
        it('THEN: pokemon details are displayed', () => {
          render(<PokemonCard wildPokemon={mockPokemon} />);

          const pokemonName = screen.getByTestId('name');
          const weight = screen.getByTestId('weight');
          const height = screen.getByTestId('height');
          const type = screen.getByTestId('pokemon-type');

          expect(pokemonName).toBeInTheDocument();
          expect(weight).toBeInTheDocument();
          expect(height).toBeInTheDocument();
          expect(type).toBeInTheDocument();
        });
      });
    });
    describe('GIVEN: no pokemon data', () => {
      it('THEN: renders an error card', () => {
        render(<PokemonCard />);
        const error = screen.getByText('Pokemon has escaped ...');
        const pokemonCard = screen.queryByTestId('pokemon-card');
        expect(error).toBeInTheDocument();
        expect(pokemonCard).not.toBeInTheDocument();
      });
    });
  });
});
