import 'jsdom-global/register';
import React from 'react';
import { render } from '@testing-library/react';
import { PokemonType } from '../pokemonType';

describe('pokemonType.jsx', () => {
  describe('PokemonType', () => {
    describe('WHEN: component is given types of pokemon', () => {
      it('THEN: component renders each type ', () => {
        const mockTypes = [
          {
            type: {
              name: 'poison',
            },
          },
          {
            type: {
              name: 'poison',
            },
          },
          {
            type: {
              name: 'poison',
            },
          },
        ];
        const { container } = render(<PokemonType types={mockTypes} />);
        const types = container.querySelectorAll('#type');
        expect(types.length).toBe(3);
      });
    });
  });
});
