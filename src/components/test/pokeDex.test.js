import 'jsdom-global/register';
import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { PokeDex } from '../pokeDex';

describe('pokeDex.js', () => {
  describe('pokeDex', () => {
    describe('GIVEN: pokeDex is given a collection of pokemon ', () => {
      it('THEN: renders the collection of pokemon', () => {
        const mockPokeDex = [
          { id: 3, name: 'wartortle' },
          { id: 5, name: 'charmander' },
          { id: 6, name: 'bulbasur' },
        ];
        render(<PokeDex pokedex={mockPokeDex} />);
        const pokDexItems = screen.queryAllByTestId('pokemon-card');
        expect(pokDexItems.length).toBe(3);
      });
    });
  });
});
