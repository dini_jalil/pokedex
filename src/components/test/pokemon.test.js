import 'jsdom-global/register';
import '@testing-library/jest-dom';
import React from 'react';
import {
  render, screen, act, fireEvent,
} from '@testing-library/react';
import { Pokemon } from '../Pokemon';
import { getNewPokemon, updatePokeDex } from '../../utils/pokemonClient';

jest.mock('../../utils/pokemonClient');

describe('pokemon.js', () => {
  describe('GIVEN: the page has loaded', () => {
    const mockPokemon = {
      name: 'bulbasaur',
      height: 7,
      weight: 69,
      sprites: {
        back_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png',
        back_female: null,
        back_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png',
        back_shiny_female: null,
        front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
        front_female: null,
        front_shiny: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png',
        front_shiny_female: null,
      },
      types: [
        {
          slot: 1,
          type: {
            name: 'grass',
            url: 'https://pokeapi.co/api/v2/type/12/',
          },
        },
        {
          slot: 2,
          type: {
            name: 'poison',
            url: 'https://pokeapi.co/api/v2/type/4/',
          },
        },
      ],
      stats: [
        {
          base_stat: 45,
          effort: 0,
          stat: {
            name: 'hp',
            url: 'https://pokeapi.co/api/v2/stat/1/',
          },
        },
        {
          base_stat: 49,
          effort: 0,
          stat: {
            name: 'attack',
            url: 'https://pokeapi.co/api/v2/stat/2/',
          },
        },
        {
          base_stat: 49,
          effort: 0,
          stat: {
            name: 'defense',
            url: 'https://pokeapi.co/api/v2/stat/3/',
          },
        },
        {
          base_stat: 65,
          effort: 1,
          stat: {
            name: 'special-attack',
            url: 'https://pokeapi.co/api/v2/stat/4/',
          },
        },
        {
          base_stat: 65,
          effort: 0,
          stat: {
            name: 'special-defense',
            url: 'https://pokeapi.co/api/v2/stat/5/',
          },
        },
        {
          base_stat: 45,
          effort: 0,
          stat: {
            name: 'speed',
            url: 'https://pokeapi.co/api/v2/stat/6/',
          },
        },
      ],
    };
    describe('WHEN: pokemon data has been fetched', () => {
      it('THEN: pokemon card , and pokedex are displayed', () => {
        render(<Pokemon />);
        const pokemonContainer = screen.getByTestId('wild-pokemon');
        const pokeDex = screen.getByTestId('pokedex');
        expect(pokemonContainer).toBeInTheDocument();
        expect(pokeDex).toBeInTheDocument();
      });
      it('THEN: pokemonData is fetched', () => {
        act(() => {
          getNewPokemon.mockResolvedValue(mockPokemon);
          render(<Pokemon />);
        });
        expect(getNewPokemon).toHaveBeenCalled();
      });
    });
    describe('GIVEN: user catches pokemon', () => {
      it('THEN: pokemon is added to users pokeDex', () => {
        act(() => {
          render(<Pokemon />);
          const catchPokemon = screen.getByTestId('catch-pokemon');
          fireEvent.click(catchPokemon);
        });
        expect(updatePokeDex).toHaveBeenCalledTimes(1);
      });
    });
  });
});
