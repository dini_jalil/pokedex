import React from 'react';
import { PokemonCard } from './pokemonCard';
import './styles/pokemonPanel.css';

export const PokemonPanel = ({ wildPokemon, newPokemon, catchPokemon }) => (
  <div data-testid="wild-pokemon">
    <div className="shake-slow">
      <PokemonCard wildPokemon={wildPokemon} />
    </div>
    <div className="pokemon-button-container">
      <button className="pokemon-button catch" data-testid="catch-pokemon" onClick={() => catchPokemon(wildPokemon)}>
        Catch
      </button>
      <button className="pokemon-button refresh" data-testid="new-pokemon" onClick={newPokemon}>
        Next Pokemon!
      </button>
    </div>
  </div>
);
