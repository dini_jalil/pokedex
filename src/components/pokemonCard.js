import React from 'react';
import { PokemonType } from './pokemonType';
import './styles/pokemonCard.css';

export const PokemonCard = ({ wildPokemon }) => {
  if (!wildPokemon) {
    return (
      <div data-testid="pokemon-card-error" className="card">
        Pokemon has escaped ...
      </div>
    );
  }

  return (
    <div className="card-container" data-testid="pokemon-card">
      <div key={wildPokemon.id} className="card">
        <div>
          <img
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${wildPokemon.id}.png`}
            width="150"
            height="150"
            alt="pokemon sprite"
          />
        </div>
        <div>
          <div>
            <div>
              <div data-testid="name">
                {wildPokemon.name}
              </div>
            </div>
            <div className="pokemon-description-pair" data-testid="weight">
              <div className="description-key">
                weight:
              </div>
              {`${wildPokemon.weight}kg`}
            </div>
            <div className="pokemon-description-pair">
              <div>
                Height:
              </div>
              <div data-testid="height">
                {`${wildPokemon.height}m`}
              </div>
            </div>
          </div>
          <div>
            {wildPokemon.types && <PokemonType types={wildPokemon.types} />}
          </div>
        </div>
      </div>
    </div>
  );
};
