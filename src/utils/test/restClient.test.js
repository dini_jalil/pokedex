import axios from 'axios';
import { getNewPokemon, updatePokeDex, removePokemon } from '../pokemonClient';

jest.mock('axios');

describe('pokemonClient.js', () => {
  describe('getPokemon()', () => {
    describe('GIVEN: function is called with invalid arguments', () => {
      it('THEN: It logs an error.', async () => {
        const mockError = new Error('error!');
        jest.spyOn(global.console, 'log').mockResolvedValueOnce(console.log(mockError));
        await getNewPokemon(3);
        expect(console.log).toBeCalledWith(mockError);
      });
    });
    describe('GIVEN: function is called with no argument', () => {
      it('THEN: fetch request is made with a random pokemon', async () => {
        jest.spyOn(Math, 'floor');
        await getNewPokemon();
        expect(Math.floor).toHaveBeenCalled();
      });
    });
    describe('GIVEN: function is called with pokemon id', () => {
      it('THEN: fetch request is made,', async () => {
        const mockPokemon = 3;
        const endpoint = `https://pokeapi.co/api/v2/pokemon/${mockPokemon}`;
        const mockAxios = jest.spyOn(axios, 'get');
        await getNewPokemon(3);
        expect(mockAxios).toBeCalledWith(endpoint);
      });
    });
  });
  describe('updatePokeDex()', () => {
    describe('GIVEN: pokemon is not contained in pokeDex', () => {
      it('THEN: returns updated pokeDex with new pokemon', () => {
        const mockPokeDex = [
          { id: 3 },
        ];
        const mockPokemon = { id: 4 };
        const result = updatePokeDex(mockPokeDex, mockPokemon);
        expect(result).toContain(mockPokemon);
      });
    });
    describe('GIVEN: pokemon is in pokeDex', () => {
      it('THEN: returns pokeDex', () => {
        const mockState = [
          { id: 3 },
        ];
        const mockPokemon = { id: 3 };
        const result = updatePokeDex(mockState, mockPokemon);
        expect(result).toHaveLength(1);
      });
    });
  });
  describe('removePokemon()', () => {
    describe('GIVEN: pokemonId to remove from pokeDex', () => {
      it('THEN: removes item from the pokeDex', () => {
        const mockPokeDex = [
          { id: 3 },
          { id: 4 },
          { id: 5 },
          { id: 6 },
        ];
        const result = removePokemon(mockPokeDex, 3);
        const expectedResult = [
          { id: 4 },
          { id: 5 },
          { id: 6 },
        ];
        expect(result).toEqual(expectedResult);
      });
    });
    describe('GIVEN: no pokeDex', () => {
      it('THEN: it returns an empty array', () => {
        const result = removePokemon(null, 3);
        expect(result).toEqual([]);
      });
    });
    describe('GIVEN:  no pokemon id', () => {
      it('THEN: it returns pokeDex', () => {
        const mockPokeDex = [
          { id: 3 },
          { id: 4 },
          { id: 5 },
          { id: 6 },
        ];
        const result = removePokemon(mockPokeDex);
        expect(result).toEqual(mockPokeDex);
      });
    });
  });
});
