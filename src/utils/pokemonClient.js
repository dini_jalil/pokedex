import axios from 'axios';

const generateRandomPokemonIndex = () => Math.floor(Math.random() * (150 - 1 + 1)) + 1;

export const getNewPokemon = async (pokemonId) => {
  const id = pokemonId || generateRandomPokemonIndex();
  const pokemon = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`);
  return pokemon;
};

const isPokemonInPokeDex = (pokeDex, pokemon) => (pokeDex.filter((p) => pokemon.id === p.id).length > 0);

export const updatePokeDex = (pokeDex, pokemon) => {
  if (isPokemonInPokeDex(pokeDex, pokemon)) {
    return pokeDex;
  }
  const updatedPokeDex = [...pokeDex, pokemon];
  return updatedPokeDex;
};

export const removePokemon = (pokeDex, pokemonId) => {
  if (!pokeDex) return [];
  if (!pokemonId) return pokeDex;
  const filteredPokeDex = pokeDex.filter((pokemon) => pokemon.id !== pokemonId);
  return filteredPokeDex;
};
