import React from 'react';
import { Pokemon } from './components/Pokemon';

function App() {
  return (
    <div className="pokemon">
      <Pokemon />
    </div>
  );
}

export default App;
